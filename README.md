# S3A_mywishlist_BIALO_TEYSSANDIER_HOLZHAMMER_FRASCHINI

Ian BIALO
David HOLZHAMMER
Théo FRASCHINI
Clément TEYSSANDIER

# Installation

Cloner le dépot
Créer la base de données avec le .sql dans ./doc/creation.sql
Créer un fichier conf.ini dans src/conf/ avec les informations nécessaires

# Conf.ini
-> copiez ce qu'il y a dessous en remplaçant ce qui est nécessaire par vos informations

[connection]
driver = mysql
host = --remplassez par le host--
database = --remplacez par le nom de la base--
username = --remplacez par le nom d'utilisateur--
password = --remplacez par le mot de passe--
charset = utf8
collation = utf8_unicode_ci
prefix =
