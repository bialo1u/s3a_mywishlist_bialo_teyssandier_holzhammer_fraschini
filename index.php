<?php
/**
 * Created by PhpStorm.
 * User: Clément Teyssandier
 * Date: 04/12/2017
 * Time: 11:03
 */
session_start();
require_once "vendor/autoload.php";

mywishlist\conf\ConnexionBase::initialisation('src/conf/conf.ini');

$app = new \Slim\Slim();

/*
 * Partie FRONT connection / deconnection / inscription
 */

$app->post('/connexion(/)', function(){
    (new mywishlist\controleurs\ControleurUser())->connexion();
})->name("connexion");

$app->post('/inscription(/)', function(){
    (new mywishlist\controleurs\ControleurUser())->inscription();
})->name("inscription");

$app->post('/liste/:no', function ($num){
    (new mywishlist\controleurs\ControleurUneListe())->afficherUneListe($num);
})->name("liste");

$app->post('/redirectListe(/)', function (){
    (new mywishlist\controleurs\ControleurUneListe())->redirigerListe();
})->name("redirectListe");

$app->post('/item/:no', function ($num){
    (new mywishlist\controleurs\ControleurUnItem())->afficherUnItem($num);
})->name("item");

$app->post('/ajoutCagnotte(/)', function (){
	(new mywishlist\controleurs\ControleurUnItem())->ajoutCagnotte();
})->name("ajoutCagnotte");

$app->post('/afficherUneListeM(/)', function (){
    (new mywishlist\controleurs\ControleurUneListe())->afficherUneListeM();
})->name("afficherUneListeM");

$app->post('/supprimerItem(/)', function (){
    (new mywishlist\controleurs\ControleurUnItem())->supprimerItem();
})->name("supprimerItem");

$app->post('/ajouterItem(/)', function (){
	(new mywishlist\controleurs\ControleurUnItem())->ajouterItem();
})->name("ajouterItem");

$app->post('/editionItem(/)',function(){
	(new mywishlist\controleurs\ControleurUnItem())->editionItemPost();
})->name("editionItemPost");

$app->post('/modification(/)',function(){
	(new mywishlist\controleurs\ControleurUser())->modification();
})->name("modification");

$app->post('/creationListe(/)',function(){
	(new mywishlist\controleurs\ControleurUneListe())->creationListe();
})->name("creation");

$app->post('/modificationListe(/)',function(){
	(new mywishlist\controleurs\ControleurUneListe())->modificationListeGenerale();
})->name("modificationListeGenerale");

$app->post('/suppressionListe(/)',function(){
	(new mywishlist\controleurs\ControleurUneListe())->suppressionListeGenerale();
})->name("suppressionListeGenerale");

$app->post('/reserverItem(/)', function (){
    (new mywishlist\controleurs\ControleurUnItem())->reserver();
})->name("reserverItem");

$app->post('/participation(/)', function (){
    (new mywishlist\controleurs\ControleurUser())->participation();
})->name("participation");

$app->post('/suppression(/)', function(){
	(new mywishlist\controleurs\ControleurUser())->suppression();
})->name("suppressionComptePost");

$app->post('/supprimerImage(/)', function (){
    (new mywishlist\controleurs\ControleurUnItem())->supprimerImage();
})->name("supprimerImage");

$app->post('/creerCagnotte(/)', function (){
	(new mywishlist\controleurs\ControleurUnItem())->creerCagnotte();
})->name("creationCagnotte");

$app->post('/ajoutImageitem(/)', function (){
    (new mywishlist\controleurs\ControleurUnItem())->ajouterImage();
})->name("ajoutImageItem");

$app->post('/modifierImageM(/)', function (){
    (new mywishlist\controleurs\ControleurUnItem())->modifierImageM();
})->name("modifierImageM");

$app->post('/modifierImage(/)', function (){
    (new mywishlist\controleurs\ControleurUnItem())->modifierImage();
})->name("modifierImage");

$app->post('/uploadImage(/)', function (){
    (new mywishlist\controleurs\ControleurUnItem())->uploadImage();
})->name("uploadImage");

$app->post('/uploadImageM(/)', function (){
    (new mywishlist\controleurs\ControleurUnItem())->uploadImageM();
})->name("uploadImageM");

$app->post('/ajouterCommentaire(/)', function (){
	(new mywishlist\controleurs\ControleurUneListe())->ajouterCommentaire();
})->name("ajouterCommentaire");


/*
 * Partie Back connection  / inscription
 */

$app->get('/', function (){
    (new mywishlist\controleurs\ControleurUser())->index();
})->name("accueil");

$app->get('/deconnexion(/)',function(){
    (new mywishlist\controleurs\ControleurUser())->deconnexion();
})->name("deconnexion");

$app->get('/connexion/',function(){
    print ((new mywishlist\vues\VueNavigation())->render(\mywishlist\vues\VueNavigation::AFF_CONNEXION));
});

$app->get('/inscription/',function(){
    print ((new mywishlist\vues\VueNavigation())->render(\mywishlist\vues\VueNavigation::AFF_INSCRIPTION));
});

$app->get('/liste/:no', function ($num){
	(new mywishlist\controleurs\ControleurUneListe())->afficherUneListe($num);
})->name("listenum");

$app->get('/item/:no/cagnotte', function ($num){
	(new mywishlist\controleurs\ControleurUnItem())->participationCagnotte($num);
})->name("accesCagnotte");

$app->get('/item/:no', function ($num){
    print ((new mywishlist\vues\VueItem())->render(\mywishlist\vues\VueItem::AFF_UN_ITEM, $num));
});

$app->get('/afficherUneListeM(/)', function (){
    print ((new mywishlist\vues\VueListe())->render(\mywishlist\vues\VueListe::AFF_LISTE_M));
});

$app->get('/listeCreateur(/)', function (){
	(new mywishlist\controleurs\ControleurUneListe())->afficherListeCreateur();
})->name("listeCreateurs");

$app->get('/editionListe/:no/ajouterItem(/)', function ($num){
	(new mywishlist\controleurs\ControleurUnItem())->creationItem($num);
})->name("ajoutItem");

$app->get('/modification(/)', function(){
    print ((new mywishlist\vues\VueNavigation())->render(\mywishlist\vues\VueNavigation::AFF_MODIFICATION));
});

$app->get('/editionListe/:no(/)',function($num){
	(new mywishlist\controleurs\ControleurUneListe())->editionListe($num);
})->name("edition");

$app->get('/editionListe/:no/parametres(/)',function($num){
	(new mywishlist\controleurs\ControleurUneListe())->editionListeGenerale($num);
})->name("editionListeGenerale");

$app->get('/editionListe/:no/editionItem/:it',function($num,$it){
	(new mywishlist\controleurs\ControleurUnItem())->editionItem($num,$it);
})->name("editionItem");

$app->get('/editionListe/:no/editionItem/:it/cagnotte',function($num,$it){
	(new mywishlist\controleurs\ControleurUnItem())->editionCagnotte($num,$it);
})->name("editionCagnotte");

$app->get('/creationListe(/)',function(){
	print ((new mywishlist\vues\VueListe())->render(\mywishlist\vues\VueListe::AFF_CREATION));
})->name("creationListe");

$app->get('/participation(/)', function (){
    print ((new mywishlist\vues\VueNavigation())->render(\mywishlist\vues\VueNavigation::AFF_PARTICIPATION));
});

$app->get('/suppression(/)', function(){
	print ((new mywishlist\vues\VueNavigation())->render(\mywishlist\vues\VueNavigation::AFF_SUPPRESSION));
})->name("suppressionCompte");

$app->get('/modifierImageM(/)', function (){
    print (new mywishlist\vues\VueItem())->render(\mywishlist\vues\VueItem::AFF_MODIF_IMAGE_M);
});

$app->get('/uploadImageM(/)', function (){
    print (new mywishlist\vues\VueItem())->render(\mywishlist\vues\VueItem::AFF_UP_IMAGE_M);
});

$app->get('/listesPubliques', function(){
    (new mywishlist\controleurs\ControleurUneListe())->listesPubliques();
})->name("listesPubliques");

$app->get('/mesListes', function(){
		(new mywishlist\controleurs\ControleurUneListe())->mesListes();
})->name("mesListes");


$app->notFound(function (){
	(new mywishlist\controleurs\Error404Controller())->affichageErreur();
});

 $app->run();