<?php
/**
 * Created by PhpStorm.
 * User: Clément Teyssandier
 * Date: 20/11/2017
 * Time: 11:30
 */

namespace mywishlist\models;


class Liste extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'liste';
    protected $primaryKey = 'no';
    public $timestamps = false;

    /**
     * @return unknown
     * 
     * Liaison avec Item
     */
    public function items(){
        return $this->hasMany('\mywishlist\models\Item', 'liste_id');
    }
    
    /**
     * @return unknown
     * 
     * Liaison avec Utilisateur
     */
    public function Utilisateur(){
    	return $this->belongsTo('\mywishlist\models\Utilisateur', 'user_id');
    }

    /**
     * @param unknown $pid
     * @return unknown
     * 
     * Récupération par ID
     */
    public static function getByID($pid){
        $id = filter_var($pid, FILTER_SANITIZE_NUMBER_INT);
        return Liste::where('no', '=', $id)->first();
    }

    /**
     * @return unknown
     * 
     * Liaison avec Commentaires
     */
    public function commentaires(){
        return $this->hasMany('\mywishlist\models\Commentaire', 'liste_id');
    }
}