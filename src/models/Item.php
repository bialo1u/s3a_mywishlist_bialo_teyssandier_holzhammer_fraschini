<?php
/**
 * Created by PhpStorm.
 * User: Clément Teyssandier
 * Date: 20/11/2017
 * Time: 11:29
 */

namespace mywishlist\models;


class Item extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'item';
    protected $primaryKey = 'id';
    public $timestamps = false;

    /**
     * @return unknown
     * 
     * Liaison avec Liste
     */
    public function liste(){
        return $this->belongsTo('\mywishlist\models\Liste', 'liste_id');
    }
    
    /**
     * @return unknown
     * 
     * Liaison avec cagnotte
     */
    public function cagnote(){
    	return $this->belongsTo('\mywishlist\models\Cagnote', 'id_cagn');
    }

    /**
     * @param unknown $pid
     * @return unknown
     * 
     * récupération par ID
     */
    public static function getByID($pid){
        $id = filter_var($pid, FILTER_SANITIZE_NUMBER_INT);
        return Item::where('id', '=', $id)->first();
    }
}