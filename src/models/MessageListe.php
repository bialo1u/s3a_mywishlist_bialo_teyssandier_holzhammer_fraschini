<?php
/**
 * Created by PhpStorm.
 * User: Clément Teyssandier
 * Date: 06/01/2018
 * Time: 15:10
 */

namespace mywishlist\models;


class MessageListe extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'messageliste';
    protected $primaryKey = 'id_message';
    public $timestamps = false;

    /**
     * @return unknown
     * 
     * Liaison avec Liste
     */
    public function liste(){
        return $this->belongsTo('\mywishlist\models\Liste', 'id_liste');
    }

    /**
     * @return unknown
     * 
     * Liaison avec Utilisateur
     */
    public function utilisateur(){
        return $this->belongsTo('\mywishlist\models\Utilisateur', 'user_id');
    }

    /**
     * @param unknown $id_message
     * @return unknown
     * 
     * récupération par ID
     */
    public static function getByID($id_message){
        $id_message = filter_var($id_message, FILTER_SANITIZE_NUMBER_INT);
        return MessageListe::where('id_message', '=', $id_message)->first();
    }
}