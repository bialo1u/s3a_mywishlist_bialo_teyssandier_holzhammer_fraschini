<?php
/**
 * Created by PhpStorm.
 * User: Clément Teyssandier
 * Date: 06/01/2018
 * Time: 15:11
 */

namespace mywishlist\models;


class Participation extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'participation';
    protected $primaryKey = 'id_participation';
    public $timestamps = false;

    /**
     * @return unknown
     * 
     * Liaison avec Liste
     */
    public function liste(){
        return $this->belongsTo('\mywishlist\models\Liste', 'id_liste');
    }

    /**
     * @return unknown
     * 
     * Liaison avec Utilisateur
     */
    public function utilisateur(){
        return $this->belongsTo('\mywishlist\models\Utilisateur', 'user_id');
    }

    /**
     * @return unknown
     * 
     * Liaison avec Message
     */
    public function message(){
        return $this->belongsTo('\mywishlist\models\MessageListe', 'id_message');
    }
}