<?php
/**
 * Created by PhpStorm.
 * User: Clément Teyssandier
 * Date: 16/01/2018
 * Time: 13:10
 */

namespace mywishlist\models;


class Commentaire extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'commentaire';
    protected $primaryKey = 'id_com';
    public $timestamps = false;

    /**
     * @return unknown
     * 
     * Liaison avec Utilisateur
     */
    public function utilisateur(){
        return $this->belongsTo('\mywishlist\models\Utilisateur', 'id_user');
    }

    /**
     * @return unknown
     * 
     * Liaison avec Liste
     */
    public function liste(){
        return $this->belongsTo('\mywishlist\models\Liste', 'liste_id');
    }
}