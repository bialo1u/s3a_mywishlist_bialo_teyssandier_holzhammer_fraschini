<?php
/**
 * Created by PhpStorm.
 * User: Clément Teyssandier
 * Date: 20/11/2017
 * Time: 11:30
 */

namespace mywishlist\models;

class Utilisateur extends \Illuminate\Database\Eloquent\Model
{
    protected $table = "utilisateur";
    protected $primaryKey = "user_id";
    public $timestamps = false;
    
    /**
     * @return unknown
     * 
     * Liaison avec Liste
     */
    public function Liste(){
    	return $this->hasMany('\mywishlist\models\Liste', 'user_id');
    }
    
    /**
     * @return unknown
     * 
     * Liaison avec Cagnotte
     */
    public function cagnote(){
    	return $this->belongsTo('\mywishlist\models\cagnote', 'id_cagn');
    }

    /**
     * @param unknown $mail
     * @return unknown
     * 
     * Récupération par Mail
     */
    public static function getByEmail($mail){
        $email = filter_var($mail, FILTER_SANITIZE_EMAIL);
        return Utilisateur::where('email','=',$email)->first();
    }

    /**
     * @param unknown $user_id
     * @return unknown
     *
     * récupération par ID d'user 
     */
    public static function getByUserID($user_id){
        $user_id = filter_var($user_id, FILTER_SANITIZE_NUMBER_INT);
        return Utilisateur::where('user_id', '=', $user_id)->first();
    }

    /**
     * @return unknown
     * 
     * Récupération des participations
     */
    public function participations(){
        return $this->hasMany('\mywishlist\models\Participation', 'user_id');
    }

}