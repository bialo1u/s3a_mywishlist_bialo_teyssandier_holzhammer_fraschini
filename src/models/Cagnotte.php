<?php
/**
 * Created by PhpStorm.
 * User: Clément Teyssandier
 * Date: 16/01/2018
 * Time: 13:10
 */

namespace mywishlist\models;


class Cagnotte extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'cagnotte';
    protected $primaryKey = 'id_cagn';
    public $timestamps = false;

    /**
     * @return unknown
     * 
     * Liaison avec Utilisateur
     */
    public function utilisateur(){
        return $this->hasMany('\mywishlist\models\Utilisateur', 'user_id');
    }

    /**
     * @return unknown
     * 
     * Liaison avec Item
     */
    public function item(){
        return $this->belongsTo('\mywishlist\models\Item', 'id_cagn');
    }
}