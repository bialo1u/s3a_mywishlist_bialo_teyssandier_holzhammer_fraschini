<?php
namespace mywishlist\conf;
use \mywishlist\models\Utilisateur;
use \mywishlist\vues\VuePageHTML;

class Authentication{

	/**
	 * @param unknown $surName
	 * @param unknown $firstName
	 * @param unknown $mail
	 * @param unknown $date
	 * @param unknown $password
	 * 
	 * Cette méthode créé un utilisateur en lui attribuant les paramètres
	 */
	public static function createUser( $surName, $firstName, $mail, $date, $password){
		$password = password_hash($password, PASSWORD_DEFAULT, Array('cost' => 12));
		$u = new Utilisateur();
		$u->email = $mail;
		$u->nom = $surName;
		$u->prenom = $firstName;
		$u->date = $date;
		$u->pass = $password;
		$u->save();
	}
	
	/**
	 * @param unknown $mail
	 * @param unknown $password
	 * 
	 * Cette fonction vérifie si les paramètres correspondent à un couple dans la base et dans ce cas connecte le compte sur navigateur
	 */
	public static function authenticate($mail, $password){
		$app =  \Slim\Slim::getInstance();
        $user = Utilisateur::getByEmail($mail);
        if($user != null) {
        	if (password_verify($password,$user->pass)) {
                self::loadProfile($user->user_id);
            } else {
                $app->redirect($app->urlFor("accueil"));
            }
        }else{
            $app->redirect($app->urlFor("accueil"));
        }
	}
	
	/**
	 * @param unknown $uid
	 * 
	 * Fonction qui va charger le profil
	 */
	public static function loadProfile( $uid ){
        $app =  \Slim\Slim::getInstance();
        $user = Utilisateur::getByUserID($uid);
        $_SESSION['email'] = $user->email;
        $app->setCookie('email', $_SESSION['email']);
        $app->redirect($app->urlFor("accueil"));
	}
	
	/**
	 * @param unknown $u
	 * @param unknown $surName
	 * @param unknown $firstName
	 * @param unknown $mail
	 * @param unknown $date
	 * @param unknown $password
	 * 
	 * Méthode qui met à jour les informations d'un profil 
	 */
	public static function updateUser( $u, $surName, $firstName, $mail, $date, $password ){
		$password = password_hash($password, PASSWORD_DEFAULT, Array('cost' => 12));
		$u->email = $mail;
		$u->nom = $surName;
		$u->prenom = $firstName;
		$u->date = $date;
		if(!empty($password)) $u->pass = $password;		
		$u->save();
		setcookie("c_user", $mail, time()+60*60*24*120);
	}
	
}