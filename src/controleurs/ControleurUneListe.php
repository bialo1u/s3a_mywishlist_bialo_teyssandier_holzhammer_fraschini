<?php
/**
 * Created by PhpStorm.
 * User: Clément Teyssandier
 * Date: 20/12/2017
 * Time: 17:15
 */

namespace mywishlist\controleurs;

use mywishlist\models\Liste;
use mywishlist\models\Utilisateur;
use mywishlist\models\Item;
use mywishlist\models\Commentaire;
use mywishlist\vues\VueListe;


class ControleurUneListe
{
	/**
	 * Méthode pour afficher la liste des créateurs
	 */
	public function afficherListeCreateur(){
		$vue = new VueListe();
		$liste = Liste::all('user_id');
		$user = Utilisateur::whereIn('user_id',$liste)->get();
		print $vue->render(VueListe::AFF_LISTE_CREATEUR, $user);
	}
	
    /**
     * @param unknown $num
     * 
     * Méthode pour afficher la liste numero $num
     */
    public function afficherUneListe($num){  	
        $vue = new VueListe();
        $num = filter_var($num, FILTER_SANITIZE_STRING);
        print $vue->render(VueListe::AFF_UNE_LISTE, $num);
    }

    /**
     * Méthode pour afficher une liste recherchée
     */
    public function afficherUneListeM(){
        $vue = new VueListe();
        print $vue->render(VueListe::AFF_LISTE_M);
    }

    /**
     * Méthode pour rediriger vers une liste
     */
    public function redirigerListe(){
        $app =  \Slim\Slim::getInstance();
        $requete = $app->request();
        $num = filter_var($requete->post('tokenListe'), FILTER_SANITIZE_STRING);
        $r_liste = $app->urlFor("liste", ['no'=>$num]);
        $app->redirect($r_liste);
    }
    
    /**
     * @param unknown $num
     * 
     * Méthode pour éditer la liste $num
     */
    public function editionListe($num){
    	$app =  \Slim\Slim::getInstance();
    		if(isset($_SESSION['email'])){
    			$user = Utilisateur::getByEmail($_SESSION['email']);
    			$id1 = $user->user_id;
    			$liste = Liste::where('no','=',$num)->first();
    			if(empty($liste)) $app->redirect($app->urlFor('accueil'));
    			else{
    				$id2 = $liste->user_id;
    				if($id1 == $id2){
    					$liste = Liste::where('no','=',$num)->first();
    					$items = Item::where('liste_id','=',$liste->no)->get();
    					$param = array($liste,$items);
    					$vue = new VueListe();
    					print $vue->render(VueListe::AFF_EDITION,$param);
    			} else $app->redirect($app->urlFor('accueil'));
    		}
    	} else $app->redirect($app->urlFor('accueil'));
    }

    /**
     * Méthode pour créer une nouvelle liste
     */
    public function creationListe(){
    	$app =  \Slim\Slim::getInstance();
    	$requete = $app->request();
    	$nom = $requete->post("nomListe");
    	$descriptif = $requete->post("descrListe");
    	$date = $requete->post("annee")."-".$requete->post("mois")."-".$requete->post("jour");
    	$proche = $requete->post("proche");
    	$public = $requete->post("public");
    	if(isset($_SESSION['email'])) $user = Utilisateur::getByEmail($_SESSION['email']);
    	else $app->redirect($app->urlFor("accueil"));
    	$liste = new Liste();
    	$liste->user_id = $user->user_id;
    	$liste->titre = $nom;
    	$liste->description = $descriptif;
    	$liste->expiration = $date;
    	if($proche == 1) $liste->pour_proche = 1;
    	else $liste->pour_proche = 0;
    	if($public == 1) $liste->Publique = 1;
    	else $liste->Publique = 0;
    	$liste->save();
    	$liste->token = 'nosecure'.$liste->no;
    	$liste->save();
    	$app->redirect($app->urlFor("edition", ['no'=>$liste->no]));
    }
    
    /**
     * @param unknown $num
     *
     * Méthode pour modifier la partie générale d'une liste (descr, titre...)
     */
    public function editionListeGenerale($num){
    	$app =  \Slim\Slim::getInstance();
    	if(isset($_SESSION['email'])){
    		$liste = Liste::getByID($num);
    		$vue = new VueListe();
    		print $vue->render(VueListe::AFF_EDITION_GENERALE,$liste);
    	} else $app->redirect($app->urlFor('accueil'));
    }
    
    /**
     * Méthode qui applique les changements effectués
     */
    public function modificationListeGenerale(){
    	$app =  \Slim\Slim::getInstance();
    	$requete = $app->request();
    	$liste = Liste::getByID($requete->post("idListe"));
    	$liste->titre = $requete->post("nomListe");
    	$liste->description = $requete->post("descrListe");
    	$liste->expiration = $requete->post("annee")."-".$requete->post("mois")."-".$requete->post("jour");
    	if($requete->post("proche") == 1) $liste->pour_proche = 1;
    	else $liste->pour_proche = 0;
    	if($requete->post("status") == 1) $liste->Publique = 1;
    	else $liste->Publique = 0;
    	$liste->save();
    	$app->redirect($app->urlFor('mesListes'));
    }
    
    /**
     * Méthode pour supprimer une liste
     */
    public function suppressionListeGenerale(){
    	$app =  \Slim\Slim::getInstance();
    	$requete = $app->request();
    	$nom = $requete->post("idListe");
    	$liste = Liste::getByID($nom);
    	$liste->delete();
    	$app->redirect($app->urlFor('mesListes'));
    }
    
    /**
     * Méthode pour récupérer les listes d'une personne connecté
     */
    public function mesListes(){
    	$app =  \Slim\Slim::getInstance();
    	if(isset($_SESSION['email'])){
    		$user = Utilisateur::getByEmail($_SESSION['email']);
    		$liste = Liste::where('user_id','=',$user->user_id)->get();
    		$vue = new VueListe();
    		print $vue->render(VueListe::AFF_MESLISTES,$liste);
    	} else $app->redirect($app->urlFor('accueil'));
    }
    
    /**
     * Méthode pour afficher les listes publiques
     */
    public function listesPubliques(){
    	$vue = new VueListe;
    	print $vue->render(VueListe::AFF_LISTESPUBLIQUES);
    }
    
    /**
     * Méthode pour ajouter un commentaire
     */
    public function ajouterCommentaire(){
    	$app =  \Slim\Slim::getInstance();
    	$requete = $app->request();
    	$liste = Liste::where('token', '=', $requete->post("token"))->first();
    	$com = new Commentaire();
    	if(isset($_SESSION['email'])){
    		$user = Utilisateur::getByEmail($_SESSION['email']);
    		$com->id_user = $user->user_id;
    		$com->nom = $user->nom;
    	} else $com->nom = filter_var($requete->post("commNom"), FILTER_SANITIZE_STRING);
    	$com->commentaire = filter_var($requete->post("commTxt"),FILTER_SANITIZE_STRING);
    	$jour = date('d');
    	$mois = date('m');
    	$annee = date('Y');
    	$date = $annee."-".$mois."-".$jour;
    	$com->date_com = $date;
    	$com->id_liste = $liste->no;
    	$com->save();
    	$app->redirect($app->urlFor('listenum',['no'=>$requete->post("token")]));
    }

}