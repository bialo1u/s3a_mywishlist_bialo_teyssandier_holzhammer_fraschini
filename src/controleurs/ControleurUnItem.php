<?php
/**
 * Created by PhpStorm.
 * User: Clément Teyssandier
 * Date: 29/12/2017
 * Time: 17:41
 */

namespace mywishlist\controleurs;


use mywishlist\models\Item;
use mywishlist\models\Liste;
use mywishlist\models\MessageListe;
use mywishlist\models\Participation;
use mywishlist\models\Utilisateur;
use mywishlist\models\Cagnotte;
use mywishlist\vues\VueItem;
use Slim\Http\UploadedFile;

class ControleurUnItem
{
    /**
     * @param unknown $num
     * 
     * Méthode pour afficher l'item $num de la liste
     */
    public function afficherUnItem($num){
        $vue = new VueItem();
        print $vue->render(VueItem::AFF_UN_ITEM, $num);
    }
    
    /**
     * @param unknown $num
     * @param unknown $it
     * 
     * Méthode pour éditer la gagnotte
     */
    public function editionCagnotte($num,$it){
    	$vue = new VueItem();
    	$liste = Liste::where('no','=',$num)->first();
    	$item = Item::where('id','=',$it)->first();
    	$param = array($liste,$item);
    	print $vue->render(VueItem::AFF_CAGNOTTE_EDITION, $param);
    }
    
    /**
     * Méthode pour créer une cagnotte sur un item
     */
    public function creerCagnotte(){
    	$app =  \Slim\Slim::getInstance();
    	$requete = $app->request();
    	$num = $requete->post("idListe");
    	$it = $requete->post("idItem");
    	$item = Item::where('id','=',$it)->first();
    	$c = new Cagnotte();
    	$c->save();
    	$item->id_cagn = $c->id_cagn;
    	$item->save();
    	$app->redirect($app->urlFor('edition',['no'=>$num]));
    }
    
    /**
     * Méthode pour afficher la cagnotte sur un item
     * @param unknown $num
     */
    public function participationCagnotte($num){
    	$app =  \Slim\Slim::getInstance();
    	$item = Item::where('id','=',$num)->first();
    	$val = Cagnotte::where('id_cagn','=',$item->id_cagn)->first();
    	$liste = Liste::where('no','=',$item->liste_id)->first();
    	$param = array($num,$item,$val,$liste);
    	$vue = new VueItem();
    	print $vue->render(VueItem::AFF_CAGNOTTE_PARTICIPATION, $param);
    }
    
    /**
     * Méthode pour ajouter de la valeur à une cagnotte
     */
    public function ajoutCagnotte(){
    	$app = \Slim\Slim::getInstance();
    	$requete = $app->request();
    	$valeur = $requete->post("valeurCagnote");
    	$item = Item::getByID($requete->post("idItem"));
    	$liste = Liste::where("no","=",$item->liste_id)->first();
    	$cagn = Cagnotte::where('id_cagn','=',$requete->post("cagnotte"))->first();
    	echo $cagn;
    	$nouvVal = $cagn->valeur+$valeur;
    	$cagn->valeur = $nouvVal;
    	$cagn->save();
    	$app->redirect($app->urlFor('listenum',['no'=>$liste->token]));
    }

    /**
     * Méthode pour réserver un item
     */
    public function reserver(){
        $app =  \Slim\Slim::getInstance();
        $requete = $app->request();
        $message = filter_var($requete->post("messageRes"), FILTER_SANITIZE_STRING);
        $nom = filter_var($requete->post("nomRes"), FILTER_SANITIZE_STRING);
        $item_id = $requete->post("itemId");
        if(isset($_SESSION['email'])){
            $user_email = $_SESSION['email'];
            $user = Utilisateur::getByEmail($user_email);
        }
        if(sizeof($message) == 0){
            if(isset($_SESSION['email'])){
                $nom = $user->prenom.' '.$user->nom;
                $m = new MessageListe();
                $m->message = $message;
                $m->id_item = $item_id;
                $m->user_id = $user->user_id;
                $m->save();
                $i = Item::getByID($item_id);
                $i->participant = $nom;
                $i->id_message = $m->id_message;
                $i->save();
                $p = new Participation();
                $p->id_item = $item_id;
                $p->user_id = $user->user_id;
                $p->id_message = $m->id_message;
                $p->save();
            } else {
                $m = new MessageListe();
                $m->message = $message;
                $m->id_item = $item_id;
                $m->save();
                $i = Item::getByID($item_id);
                $i->participant = $nom;
                $i->id_message = $m->id_message;
                $i->save();
            }
        } else {
            if(isset($_SESSION['email'])){
                $nom = $user->prenom.' '.$user->nom;
                $i = Item::getByID($item_id);
                $i->participant = $nom;
                $i->save();
                $p = new Participation();
                $p->id_item = $item_id;
                $p->user_id = $user->user_id;
                $p->save();
            } else {
                $i = Item::getByID($item_id);
                $i->participant = $nom;
                $i->save();
            }
        }
        $r_item = $app->urlFor("item", ['no'=>$item_id]);
        $app->redirect($r_item);
    }

    /**
     * Méthode pour supprimer un item
     */
    public function supprimerItem(){
        $app =  \Slim\Slim::getInstance();
        $requete = $app->request();
        $id_i = $requete->post('itemIdSupp');
        $i = Item::getByID($id_i);
        if(isset($i->id_message)){
            $m = MessageListe::getByID($i->id_message);
            $m->delete();
        }
        if(isset($i->participant)){
            $p = Participation::where('id_item', '=', $id_i);
            $p->delete();
        }
        $i->delete();
        $r_liste = $app->urlFor("edition",['no'=>$i->liste_id]);
        $app->redirect($r_liste);
    }

    /**
     * Méthode pour supprimer une imgage associée sur un item
     */
    public function supprimerImage(){
        $app =  \Slim\Slim::getInstance();
        $requete = $app->request();
        $id_item = $requete->post('itemIdSupp');
        $i = Item::getByID($id_item);
        $i->img = null;
        $i->save();
        $r_item = $app->urlFor("item", ['no'=>$id_item]);
        $app->redirect($r_item);
    }

    /**
     * @param unknown $num
     * 
     * Méthode pour créer un item
     */
    public function creationItem($num){
    	$app =  \Slim\Slim::getInstance();
    	if(isset($_SESSION['email'])){
    		$user = Utilisateur::getByEmail($_SESSION['email']);
    		$id1 = $user->user_id;
    		$liste = Liste::where('no','=',$num)->first();
    		if(empty($liste)) $app->redirect($app->urlFor('accueil'));
    		else{
    			$id2 = $liste->user_id;
    			if($id1 == $id2){
    				$vue = new VueItem();
    				print $vue->render(VueItem::AFF_AJOUT_ITEM, $num);
    			} else $app->redirect($app->urlFor('accueil'));
    		}
    	} else {
    	    $app->redirect($app->urlFor('accueil'));
        }
    }
    
    /**
     * @param unknown $num
     * @param unknown $it
     * 
     * Méthode pour éditer un item existant
     */
    public function editionItem($num,$it){
    	$vue = new VueItem();
    	$item = Item::where('id','=',$it)->first();
    	if($item->descr == '[Aucun]') $item->descr = "";
    	$param = array($num,$item);
    	print $vue->render(VueItem::AFF_MODIF_ITEM,$param);
    }
    
    /**
     * Méthode pour valider les changements et les effectuer dans la base
     */
    public function editionItemPost(){
    	$app =  \Slim\Slim::getInstance();
    	$requete = $app->request();
    	$item = Item::getByID($requete->post('idItem'));
    	$item->nom = filter_var($requete->post('nomItem'),FILTER_SANITIZE_STRING);
    	$item->descr = filter_var($requete->post('descrItem'),FILTER_SANITIZE_STRING);
   		if(empty($requete->post('descrItem'))) $item->descr= "[Aucun]";
   		$item->tarif = filter_var($requete->post('prixItem'),FILTER_SANITIZE_NUMBER_FLOAT);
   		$item->save();
   		$app->redirect($app->urlFor('edition',['no'=>$requete->post('idListe')]));
    }

    /**
     * Méthode pour ajouter un item
     */
    public function ajouterItem(){
    	$app =  \Slim\Slim::getInstance();
    	$requete = $app->request();
    	$idListe = $requete->post("idListe");
    	$nomItem = $requete->post("nomItem");
    	$descrItem = $requete->post("descrItem");
    	if(empty($descrItem)) $descrItem = "[Aucun]";
    	$prix = $requete->post("prixItem");
    	$item = new Item();
    	$item->liste_id = filter_var($idListe,FILTER_SANITIZE_NUMBER_INT);
    	$item->nom = filter_var($nomItem,FILTER_SANITIZE_STRING);
    	$item->descr = filter_var($descrItem,FILTER_SANITIZE_STRING);
    	$item->tarif = filter_var($prix,FILTER_SANITIZE_NUMBER_FLOAT);
    	$item->save();
    	$app->redirect($app->urlFor("edition",['no'=>$idListe]));
    }

    /**
     * Méthode pour ajouter une image
     */
    public function ajouterImage(){
        $app =  \Slim\Slim::getInstance();
        $requete = $app->request();
        $id_item = $requete->post("itemIdAj");
        $i = Item::getByID($id_item);
        $img = $requete->post("imageAj");
        $i->img = $img;
        $i->save();
        $r_item = $app->urlFor("item", ['no'=>$id_item]);
        $app->redirect($r_item);
    }

    /**
     * Méthode pour afficher le menu de modification d'image
     */
    public function modifierImageM(){
        $vue = new VueItem();
        print $vue->render(VueItem::AFF_MODIF_IMAGE_M);
    }

    /**
     * Méthode pour modifier une image
     */
    public function modifierImage(){
        $app = \Slim\Slim::getInstance();
        $requete = $app->request();
        $id_item = $requete->post("itemIdMod");
        $i = Item::getByID($id_item);
        $img = filter_var($requete->post("imageMod"), FILTER_SANITIZE_STRING);
        $i->img = $img;
        $i->save();
        $r_item = $app->urlFor("item", ['no'=>$id_item]);
        $app->redirect($r_item);
    }

    /**
     * Méthode pour uploader une image
     */
    public function uploadImage(){
        $app =  \Slim\Slim::getInstance();
        $requete = $app->request();
        $id_item = $requete->post("itemIdUpM");
        $file_name = $_FILES['uploadIm']['name'];
        $file_type = strtolower(  substr(  strrchr($file_name, '.')  ,1));
        $erreur = "Il n'y a aucune erreur";
        $extensions_valides = array( 'jpg' , 'jpeg' , 'gif' , 'png' );
        if(!in_array($file_type, $extensions_valides)){
            $erreur = "Extension incorrecte";
        }
        if($_FILES['uploadIm']['error'] > 0){
            $erreur = "Erreur lors du transfert";
        }
        $file_name = md5(uniqid(rand(), true))."jpg";
        if($erreur == "Il n'y a aucune erreur"){
            $i = Item::getByID($id_item);
            $i->img = $file_name;
            $i->save();
            move_uploaded_file($_FILES['uploadIm']['tmp_name'], "doc/Images/".$file_name);
        }
        $r_item = $app->urlFor("item", ['no'=>$id_item]);
        $app->redirect($r_item);
    }

    /**
     * Méthode pour afficher le menu d'upload d'image
     */
    public function uploadImageM(){
        $vue = new VueItem();
        print $vue->render(VueItem::AFF_UP_IMAGE_M);
    }
}