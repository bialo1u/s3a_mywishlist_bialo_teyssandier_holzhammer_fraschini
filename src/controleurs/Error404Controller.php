<?php

namespace mywishlist\controleurs;
use mywishlist\vues\VueError404;

class Error404Controller {

	public function __construct() {}

	/**
	 * Méthode pour gérer l'erreur 404
	 */
	public function affichageErreur() {
		$vue = new VueError404();
		echo $vue->render();
	}

}