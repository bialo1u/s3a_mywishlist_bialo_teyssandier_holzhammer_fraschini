<?php
/**
 * Created by PhpStorm.
 * User: Clément Teyssandier
 * Date: 11/12/2017
 * Time: 11:39
 */
namespace mywishlist\controleurs;

use mywishlist\models\Utilisateur;
use \mywishlist\vues\VueNavigation;
use \mywishlist\conf\Authentication;
use \mywishlist\vues\VuePageHTML;

class ControleurUser
{
    /**
     * Méthode pour afficher le rendu
     */
    public function index(){
        $vue = new VueNavigation;
        print $vue->render(VueNavigation::AFF_INDEX);
    }

    /**
     * Méthode pour se connecter sur le site
     */
    public function connexion(){
        $app =  \Slim\Slim::getInstance();
        $requete = $app->request;
        $password = $requete->post("mdpCo");
        $mail = filter_var($requete->post("mailCo"),FILTER_SANITIZE_EMAIL);
        
        Authentication::authenticate($mail, $password);
    }

    /**
     * Méthode pour se déconnecter
     */
    public function deconnexion(){
        session_destroy();
        $app =  \Slim\Slim::getInstance();
        $app->redirect($app->urlFor("accueil"));
    }

    /**
     * Méthode pour s'inscrire, enregistre dans la base le compte si valide
     */
    public function inscription(){
		$app =  \Slim\Slim::getInstance();
    	$requete = $app->request;
   		$nom = $requete->post("nomInscr");
   		$nom = filter_var($nom,FILTER_SANITIZE_STRING);
   		$prenom = $requete->post("prenomInscr");
   		$prenom = filter_var($prenom,FILTER_SANITIZE_STRING);
   		$mail = $requete->post("mailInscr");
   		$mail = filter_var($mail,FILTER_SANITIZE_EMAIL);
   		$date = $requete->post("annee")."-".$requete->post("mois")."-".$requete->post("jour");
   		$mdp1 = $requete->post("mdp1Inscr");
   		$mdp2 = $requete->post("mdp2Inscr");
   		
   		if(!filter_var($nom,FILTER_SANITIZE_STRING) || !filter_var($prenom,FILTER_SANITIZE_STRING) || !filter_var($mail,FILTER_SANITIZE_EMAIL)){
   			$app->redirect($app->urlFor("accueil"));
   		}
   		if(!checkdate($requete->post("mois"),$requete->post("jour"),$requete->post("annee"))){
   			$app->redirect($app->urlFor("accueil"));
   		}
   		if(!filter_var($mdp1,FILTER_SANITIZE_STRING) || !filter_var($mdp2,FILTER_SANITIZE_STRING) || $mdp1 != $mdp2){
   			$app->redirect($app->urlFor("accueil"));
   		}   		
   		Authentication::createUser($nom,$prenom,$mail,$date,$mdp1);
   		$_SESSION['email'] = $mail;
   		$app->redirect($app->urlFor("accueil"));
    }
    
    /**
     * Méthode pour modifier les informations du profil utilisateur
     */
    public function modification(){
    	$app =  \Slim\Slim::getInstance();
    	$requete = $app->request;
    	$nom = $requete->post("nomInscr");
    	$nom = filter_var($nom,FILTER_SANITIZE_STRING);
    	$prenom = $requete->post("prenomInscr");
    	$prenom = filter_var($prenom,FILTER_SANITIZE_STRING);
    	$mail = $requete->post("mailInscr");
    	$mail = filter_var($mail,FILTER_SANITIZE_EMAIL);
    	$date = $requete->post("annee")."-".$requete->post("mois")."-".$requete->post("jour");
    	$mdp0 = $requete->post("mdp0Inscr");
    	$mdp1 = $requete->post("mdp1Inscr");
    	$mdp2 = $requete->post("mdp2Inscr");
    	$user = Utilisateur::getByEmail($_SESSION['email']);
    	if(!filter_var($nom,FILTER_SANITIZE_STRING) || !filter_var($prenom,FILTER_SANITIZE_STRING) || !filter_var($mail,FILTER_SANITIZE_EMAIL)){
    		$app->redirect($app->urlFor("accueil"));
    	}
    	if(!checkdate($requete->post("mois"),$requete->post("jour"),$requete->post("annee"))){
    		$app->redirect($app->urlFor("accueil"));
    	}
    	if(empty($mdp0) && empty($mdp1) && empty($mdp2)){
    		Authentication::updateUser($user,$nom,$prenom,$mail,$date,null);
    		$_SESSION['email'] = $mail;
    		$app->redirect($app->urlFor("accueil"));   		
    	} else {
    		if(!password_verify($mdp0,$user->pass)){
    			echo 'test1';
    			$app->redirect($app->urlFor("accueil"));
    		} else {
    			if(!filter_var($mdp1,FILTER_SANITIZE_STRING) || !filter_var($mdp2,FILTER_SANITIZE_STRING) || $mdp1 != $mdp2){
    				echo 'test2';
    				$app->redirect($app->urlFor("accueil"));
    			} else {
    				echo 'test3';
    				Authentication::updateUser($user,$nom,$prenom,$mail,$date,$mdp1);
    				$_SESSION['email'] = $mail;
    				$app->redirect($app->urlFor("accueil"));
    			}
    		}
    	}
    }

    /**
     * Méthode pour afficher les participations du compte
     */
    public function participation(){
        $vue = new VueNavigation;
        print $vue->render(VueNavigation::AFF_PARTICIPATION);
    }
    
    /**
     * Méthode pour supprimer le compte
     */
    public function suppression(){
    	$app =  \Slim\Slim::getInstance();
    	if(isset($_SESSION['email'])){
    		$mail = $_SESSION['email'];
    		Authentication::deleteUser($mail);
    	}
    	$app->redirect($app->urlFor("accueil"));
    }
}