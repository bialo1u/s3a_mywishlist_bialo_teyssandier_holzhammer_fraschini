<?php
/**
 * Created by PhpStorm.
 * User: Clément Teyssandier
 * Date: 05/01/2018
 * Time: 23:54
 */

namespace mywishlist\vues;

use mywishlist\models\Liste;
use mywishlist\models\Utilisateur;
use mywishlist\models\Commentaire;

class VueListe
{

    const AFF_UNE_LISTE = 1;
    const AFF_LISTE_M = 2;
    const AFF_EDITION = 3;
    const AFF_CREATION = 4;
    const AFF_MESLISTES = 5;
    const AFF_LISTESPUBLIQUES = 6;
    const AFF_EDITION_GENERALE = 7;
    const AFF_LISTE_CREATEUR = 8;

    /**
     * @param unknown $selecteur
     * @param unknown $num
     * @return string
     * 
     * Méthode d'affichage en fonction des cas
     */
    public function render($selecteur,$num = null)
    {
        $content=null;
        switch ($selecteur) {
            case VueListe::AFF_UNE_LISTE :
                $content = $this->une_Liste($num);
                break;
            case VueListe::AFF_LISTE_M :
                $content = $this->une_Liste_M();
                break;
            case VueListe::AFF_EDITION :
            	$content = $this->editionListe($num);
            	break;
            case VueListe::AFF_CREATION :
            	$content = $this->creationListe();
            	break;
            case VueListe::AFF_MESLISTES :
            	$content = $this->afficherListes($num);
            	break;
            case VueListe::AFF_LISTESPUBLIQUES:
            	$content = $this->afficher_Listes_Publiques();
            	break;
            case VueListe::AFF_EDITION_GENERALE:
            	$content = $this->editionListeGenerale($num);
            	break;
            case VueListe::AFF_LISTE_CREATEUR:
            	$content = $this->afficherListeCreateur($num);
            	break;
        }
        return VuePageHTML::getHeaders().$content.VuePageHTML::getFooter();
    }

    /**
     * @param unknown $num
     * @return string|unknown
     * 
     * retourne l'affichage pour afficher la liste $num
     */
    private function une_Liste($num){
        $app = \Slim\Slim::getInstance();
        $content = "<h1>Page de la liste $num :</h1>";
        $liste = \mywishlist\models\Liste::where('token', '=', $num)->first();
        if(count($liste) < 1){
            return $content."<p>Aucune liste ne correspond au token saisi</p>";
        }
        $createur = Utilisateur::getByUserID($liste->user_id);
        $mail_createur = $createur->email;
        if(!isset($liste)){
            $content .= "<p>Aucune liste ne correspond au token saisi</p>";
        } else {
            $id = $liste->no;
            $items = \mywishlist\models\Item::where('liste_id', '=', $id)->get();
            if(empty ($items)){
                $content .= "<p>La liste est vide</p>";
            } else {
                foreach ($items as $item){
                    $itemID = $item->id;
                    $r_item = $app->urlFor("item", ['no'=>$itemID]);
                    $content .= '<br> <b>Item numéro :</b> '."<a href=\"$r_item\"> $itemID </a>".' '.$item->nom.' '.
                        $item->descr.' '. $item->url.' '.$item->tarif;
                    if(isset($item->participant))
                    {
                        if(isset($_SESSION['email'])){
                            if($_SESSION['email'] == $mail_createur){
                                $u = Utilisateur::getByEmail($mail_createur);
                                if($u->pour_proche == 0){
                                    $dateCurrent = date("Y-m-d");
                                    $dateExpiration = $liste->expiration;
                                    $dateExpiration = strtotime($dateExpiration);
                                    $dateCurrent = strtotime($dateCurrent);
                                    if($dateExpiration - $dateCurrent < 0){
                                        $content .= "<p><b>Avec comme participant : </b> $item->participant</p>";
                                    } else {
                                        $content .= "<p><b>Il y a un participant mais c'est une surprise</b></p>";
                                    }
                                } else {
                                    $content .= ' <b>avec comme participant :</b> '.$item->participant;
                                }
                            } else {
                                $content .= ' <b>avec comme participant :</b> '.$item->participant;
                            }
                        } else {
                            $content .= ' <b>avec comme participant :</b> '.$item->participant;
                        }
                    } else {
                        $content .= '<b> sans participant </b>';
                    }
                    if(!isset($_SESSION['email'])){
                    	if(sizeof($item->id_cagn) != 0 && $item->id_cagn!= 0){
                    		$cagn = $app->urlFor('accesCagnotte',['no'=>$item->id]);
                    		$content .= '<a href='.$cagn.'>Cagnotte disponible</a>';
                    	}
                    }else{
                    	if($_SESSION['email'] != $mail_createur){
                    		if(sizeof($item->id_cagn) != 0 && $item->id_cagn!= 0){
                    			$cagn = $app->urlFor('accesCagnotte',['no'=>$item->id]);
                    			$content .= '<a href='.$cagn.'>Cagnotte disponible</a>';
                    		}
                    	}
                    }
                    $content .= '<br>';
                }
            }
        }
        if(isset($_SESSION['email'])){
            if($_SESSION['email'] == $mail_createur){
                $url = $_SERVER['HTTP_HOST'].$app->urlFor("liste", ['no'=>$num]);
                $content .= '<br><h3>Pour partager cette liste il vous suffit d\'envoyer le lien ce dessous à vos amis</h3>'."$url";
            }
        }
        $content.=$this->commentaire($liste,$num);
        return $content;
    }
    
    /**
     * @param unknown $liste
     * @param unknown $num
     * @return string
     * 
     * Méthode pour afficher (retourne) un commentaire sur une liste
     */
    private function commentaire($liste,$num){
    	$app = \Slim\Slim::getInstance();
    	$commentaire = $app->urlFor("ajouterCommentaire");
    	$id = $liste->no;
    	$commentaires = Commentaire::where("id_liste","=",$id)->orderBy('date_com','DESC')->get();
    	$affichage = <<<end
        <h1>Section commentaire</h1>
        
        <form id="form_une_liste_m" class="formulaire" method="POST" action="$commentaire">
            <div class="row">
                <div class="input-field">
end;
    	if(!isset($_SESSION['email'])){
    		$affichage .= <<<end
			<label class="black-text">Insérez votre nom ici :</label>
            <input placeholder="Votre nom ici" type="text" name="commNom" id="commNom" required><br>
end;
    	}
    	if(count($commentaires) != 0){
    		$affichage .= <<<end
                    <label class="black-text">Insérez votre commentaire ici :</label>
                    <input placeholder="Votre commentaire ici" type="text" name="commTxt" id="commTxt" required>
                </div>
             </div>
			<input id="token" name="token" type="hidden" value=$num>
            <button type="submit" name="creerComm" value="creerComm">Valider</button>
        </form>
		<br>
end;
    	}else{
    		$affichage .= <<<end
                    <label class="black-text">Soyez le premier commentaire :</label>
                    <input placeholder="Votre commentaire ici" type="text" name="commTxt" id="commTxt" required>
                </div>
             </div>
			<input id="token" name="token" type="hidden" value=$num>
            <button type="submit" name="creerComm" value="creerComm">Valider</button>
        </form>
		<br>
end;
    	}
		foreach ($commentaires as $com){
			list($anneeUser,$moisUser,$jourUser) = explode('-',$com->date_com);
			$affichage .= <<<end
        <h3>Commentaire de $com->nom publié le $jourUser/$moisUser/$anneeUser</h3>
            <div class="row">
                <div class="input-field">
                    <label class="black-text">$com->commentaire</label>
                </div>
             </div>
            <br>
end;
    	}
		return $affichage;
    }
    
    /**
     * @param unknown $users
     * @return string
     * 
     * méthode afficher le créateur d'une liste
     */
    private function afficherListeCreateur($users){
    	$app = \Slim\Slim::getInstance();
    	$retour = $app->urlFor("accueil");
    	$affichage = "<h1>Liste des créateurs ayant créé au moins une liste</h1>";
    	if(count($users) == 0) $affichage.="<label class='black-text'>Aucun (pour l'instant...)</label><br>";
    	else{
    		foreach($users as $user){
    			$affichage.='<label class="black-text">'.$user->nom.' '.$user->prenom.'</label><br>';
    		}
    	}
    	$affichage .="<br><a href=$retour>Retour</a>";
    	return $affichage;
    }

    /**
     * @return string
     *
     * Affichage du menu d'une liste
     */
    private function une_Liste_M(){
        $app = \Slim\Slim::getInstance();
        $r_une_liste = $app->urlFor("redirectListe");
        return <<<end
        <h1>Affichage d'une liste</h1>
        
        <form id="form_une_liste_m" class="formulaire" method="POST" action="$r_une_liste">
            <div class="row">
                <div class="input-field">
                    <label class="black-text">Entrez le code de la liste : </label>
                    <input placeholder="ABCDEF" type="text" name="tokenListe" id="token_liste" required>
                </div>
             </div>
            <br><br>
            <button type="submit" name="Voir_Liste" value="formUneListeM">Voir la liste</button>
        </form>
        
end;
    }
        
    //Version utlisée actuellement
    /**
     * @param unknown $param
     * @return string
     * 
     * Méthode pour afficher toutes vos listes 
     */
    private function afficherListes($param){
    	$app = \Slim\Slim::getInstance();
    	$listes = $param;
    	$ajout = $app->urlFor("creationListe");
    	$supp = $app->urlFor('suppressionListeGenerale');
    	if(count($param) == 0){
    		$affichage = <<<end
		<h1>Menu d'édition des listes"</h1>
		<label class="black-text">Aucune liste n'a été trouvée</label><br><br>
		<a href=$ajout>Ajouter une liste</a>
end;
    	}else{
    	$affichage = <<<end
		<h1>Menu d'édition des listes"</h1>
		<table>
  				<tr>
				 <th>Options</th>
   				 <th>Titre</th>
    			 <th>Description</th>
   				 <th>Prix</th>
				 <th>Destinataire</th>
				 <th>Status</th>
  				</tr>
end;
    	foreach($listes as $liste){
    		list($anneeUser,$moisUser,$jourUser) = explode('-',$liste->expiration);
    		$date = $jourUser."/".$moisUser."/".$anneeUser;
    		$id = $liste->no;
    		$edit = $app->urlFor('editionListeGenerale',['no'=>$liste->no]);
    		$editItem = $app->urlFor("edition",['no'=>$liste->no]);
    		$dest = $liste->pour_proche;
    		$acces = $app->urlFor("liste", ['no'=>$liste->token]);
    		if($dest == 1) $dest = "Proche";
    		else $dest = "Perso";
    		$status = $liste->Publique;
    		if($status == 1) $status = "Publique";
    		else $status = "Privé";
    		$affichage .= <<<end
			<form id="form_une_liste_m" class="formulaire" method="POST" action="$supp">
				<tr>
				 <td id="tab_td">
				    <a href="$acces">&#9755;</a>
                     <a href=$editItem>&#9758;</a>
                     <a href=$edit>&#9881;</a>
                     <button type="submit" name='itemIdSupp' value='$liste->no'>-</button>
				 </td>
    			 <td>$liste->titre</td>
    			 <td>$liste->description</td>
    			 <td>$date</td>
				 <td>$dest</td>
				 <td>$status</td>
  				</tr>
				<input id="idListe" name="idListe" type="hidden" value=$id>
        	</form>
end;
    	}
    	$affichage .= <<<end
		</table>
		<a href=$ajout>Ajouter une liste</a>
end;
    	}
    	return $affichage;
    }
    
    /**
     * @param unknown $param
     * @return string
     * 
     * Méthode pour modifier une de vos listes
     */
    private function editionListe($param){
    	$app = \Slim\Slim::getInstance();
    	$liste = $param[0];
    	$items = $param[1];
    	$items = $param[1];
    	$ajout = $app->urlFor("ajoutItem",['no'=>$liste->no]);
    	$retour = $app->urlFor("mesListes");
    	$supp = $app->urlFor('supprimerItem');
    	if(sizeof($items) == 0){
    		$affichage = <<<end
    		<h1>Menu d'édition des listes</h1>
			<label class="black-text">Aucune item n'a été trouvé</label><br><br>
			<a href=$ajout>Ajouter un item </a>
			<a href=$retour>Retour à mes listes</a>
end;
    	}else{
    	$affichage = <<<end
		<h1>Menu d'édition de la liste "$liste->titre"</h1>
		<table>
  				<tr>
  				 <th>Id</th>
				 <th>Options</th>
   				 <th>Titre</th>
    			 <th>Description</th>
   				 <th>Prix</th>
				 <th>Cagnote</th>
  				</tr>
end;
    	foreach($items as $item){
    		$edit = $app->urlFor('editionItem',['no'=>$liste->no,'it'=>$item->id]);
    		$r_id = $app->urlFor('item', ['no'=>$item->id]);
    		$cagnotte = $app->urlFor('editionCagnotte',['no'=>$liste->no,'it'=>$item->id]);
    		if(sizeof($item->id_cagn) == 0 ||  $item->id_cagn==0) $cagn = "Non";
    		else $cagn = "Oui";
    		$affichage .= <<<end
			<form id="form_une_liste_m" class="formulaire" method="POST" action="$supp">
				<tr>
				 <td><a href="$r_id">$item->id</a></td>
				 <td id ="tab_td"><button type="submit" name='itemIdSupp' value='$item->id'>-</button>
				 <a href="$cagnotte">€</a>
				 <a href=$edit>&#9881;</a></td>
    			 <td>$item->nom</td>
    			 <td>$item->descr</td>
    			 <td>$item->tarif</td>
				 <td>$cagn</td>
  				</tr>
        	</form>
end;
    	}
    	$affichage .= <<<end
		</table>
		<br>
		<a href=$ajout>Ajouter un item </a>
		<a href=$retour> Retour à mes listes</a>
end;
    	}
    	return $affichage;
    }
        
    /**
     * @return string
     * 
     * Méthode pour créer une liste
     */
    private function creationListe(){
    	$app = \Slim\Slim::getInstance();
    	$jour = date('d');
    	$mois = date('m');
    	$annee = date('Y');
    	$creation = $app->urlFor("creation");
    	$affichage = <<<end
		<h1>Menu de création d'une nouvelle liste</h1>
		<form id="form_une_liste_m" class="formulaire" method="POST" action="$creation">
            <div class="row">
                <div class="input-field">
                    <label class="black-text">Entrez le nom de la liste : </label>
                    <input placeholder="Nom" type="text" name="nomListe" id="nom_liste" required>
                </div>
				<br>
				<div class="input-field">
                    <label class="black-text">Entrez un descriptif à la liste : </label>
                    <input placeholder="Liste pour..." type="text" name="descrListe" id="descr_liste">
                </div>
				<br>
				<label class="black-text">Date d'expiration (JJ/MM/AAAA)</label>
			<div class="input-field">
                <select id="inscription_jour" name="jour">
end;
    	for ($i=1; $i<=31; $i++) {
    		if($i == $jour){
    			$affichage = $affichage . "<option value=\"$i\" selected>$i</option>\n";
    		} else {
    			$affichage = $affichage . "<option value=\"$i\">$i</option>\n";
    		}
    	}
    	$affichage = $affichage . "</select><select id='inscription_mois' name='mois'>";
    	for ($i=1; $i<=12; $i++) {
    		if($i == $mois){
    			$affichage = $affichage . "<option value=\"$i\" selected>$i</option>\n";
    		} else {
    			$affichage = $affichage . "<option value=\"$i\">$i</option>\n";
    		}
    	}
    	$affichage = $affichage . "</select><select id='inscription_annee' name='annee'>";
    	for ($i=date('Y'); $i<=date('Y')+30; $i++) {
    		if($i == $annee){
    			$affichage = $affichage . "<option value=\"$i\" selected>$i</option>\n";
    		} else {
    			$affichage = $affichage . "<option value=\"$i\">$i</option>\n";
    		}
    	}
$affichage = $affichage . <<<end
</select>
            </div>
            </div>
			<br>
			<input type="checkbox" name="proche" value="1"/>Pour proche?<br>
    		<input type="checkbox" name="public" value="1"/>Publique?<br>
            <br/><br/>
            <button type="submit" name="creerListe" value="creerListe">Créer la liste</button>
        </form>    			
end;
    	return $affichage;
    }
    
    /**
     * @param unknown $param
     * @return string
     * 
     * Méthode pour afficher l'edition générale d'une liste
     */
    private function editionListeGenerale($param){
    	$app = \Slim\Slim::getInstance();
    	$id = $param->no;
    	list($anneeUser,$moisUser,$jourUser) = explode('-',$param->expiration);
    	$modif = $app->urlFor("modificationListeGenerale");
    	$affichage = <<<end
		<h1>Menu d'édition de la liste</h1>
		<form id="form_une_liste_m" class="formulaire" method="POST" action="$modif">
            <div class="row">
                <div class="input-field">
                    <label class="black-text">Entrez le nom de la liste : </label>
                    <input placeholder="Nom" type="text" name="nomListe" value="$param->titre" id="nom_liste" required>
                </div>
				<br>
				<div class="input-field">
                    <label class="black-text">Entrez un descriptif à la liste : </label>
                    <input placeholder="Liste pour..." type="text" name="descrListe" value="$param->description" id="descr_liste">
                </div>
				<br>
				<label class="black-text">Date d'expiration (JJ/MM/AAAA)</label>
			<div class="input-field">
                <select id="inscription_jour" name="jour">
end;
    	for ($i=1; $i<=31; $i++) {
    		if($i == $jourUser){
    			$affichage = $affichage . "<option value=\"$i\" selected>$i</option>\n";
    		} else {
    			$affichage = $affichage . "<option value=\"$i\">$i</option>\n";
    		}
    	}
    	$affichage = $affichage . "</select><select id='inscription_mois' name='mois'>";
    	for ($i=1; $i<=12; $i++) {
    		if($i == $moisUser){
    			$affichage = $affichage . "<option value=\"$i\" selected>$i</option>\n";
    		} else {
    			$affichage = $affichage . "<option value=\"$i\">$i</option>\n";
    		}
    	}
    	$affichage = $affichage . "</select><select id='inscription_annee' name='annee'>";
    	for ($i=date('Y'); $i<=date('Y')+30; $i++) {
    		if($i == $anneeUser){
    			$affichage = $affichage . "<option value=\"$i\" selected>$i</option>\n";
    		} else {
    			$affichage = $affichage . "<option value=\"$i\">$i</option>\n";
    		}
    	}
    	$affichage = $affichage . <<<end
</select>
            </div>
            </div>
			<br>
end;
    	if($param->pour_proche == 1) $affichage.="<input type='checkbox' name='proche' value='1' checked = 'checked'/>Pour proche?<br>";
    	else $affichage.='<input type="checkbox" name="proche" value="1"/>Pour proche?<br>';
    	if($param->Publique == 1) $affichage.='<input type="checkbox" name="public" value="1" checked = "checked"/>Publique?<br>';
    	else $affichage.='<input type="checkbox" name="public" value="1"/>Publique?<br>';
    		$affichage = $affichage . <<<end
            <br/><br/>
			<input id="idListe" name="idListe" type="hidden" value=$id>
            <button type="submit" name="modifListe" value="modifListe">Modifier la liste</button>
        </form>
end;
    		return $affichage;
    }
    
    /**
     * @return string
     * 
     * Méthode pour afficher les listes publiques
     */
    private function afficher_Listes_Publiques(){
    	$app = \Slim\Slim::getInstance();
    	$toutesLesListes = Liste::where('Publique', '=', 1)->get();
    	$content = "<h1>Voici la listes des listes publiques :</h1><br><br>";
    	if(count($toutesLesListes)>0){
    		foreach($toutesLesListes as $li){
    			$owner = Utilisateur::getByUserID($li->user_id);
    			$content .= "Cette liste appartient à $owner->prenom $owner->nom <br>Informations :<br>";
    			$content .= "$li->titre  |  $li->description  |  Expire le $li->expiration  |";
    			if($li->pour_proche == 0){
    				$content .= "  Pour le créateur";
    			}else{
    				$content .= "  Pour un proche";
    			}
    			$content .= "<a href=$app->urlFor('accueil')> Accéder</a><br><br>";
    		}
    	}else{
    		$content .= "Il n'y a actuellement aucune liste publique.<br>";
    	}
    	return $content;
    }
}