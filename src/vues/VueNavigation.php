<?php
/**
 * Created by PhpStorm.
 * User: Clément Teyssandier
 * Date: 11/12/2017
 * Time: 11:42
 */

namespace mywishlist\vues;

use mywishlist\models\Item;
use mywishlist\models\Liste;
use mywishlist\models\MessageListe;
use mywishlist\models\Utilisateur;

class VueNavigation
{
    const AFF_INDEX = 1;
    const AFF_CONNEXION = 2;
    const AFF_INSCRIPTION = 3;
    const AFF_PARTICIPATION = 4;
    const AFF_MODIFICATION = 6;
    const AFF_SUPPRESSION = 7;

    private $objet;

    public function __construct($array = null)
    {
        $this->objet = $array;
    }

    /**
     * @param unknown $selecteur
     * @return string
     * 
     * Méthode pour afficher les pages en fonction des cas
     */
    public function render($selecteur)
    {
    	$content=null;
        switch ($selecteur) {
            case VueNavigation::AFF_INDEX :
                $content = $this->index();
                break;
            case VueNavigation::AFF_CONNEXION :
                $content = $this->connexion();
                break;
            case VueNavigation::AFF_INSCRIPTION :
                $content = $this->inscription();
                break;
            case VueNavigation::AFF_MODIFICATION :
            	$content = $this->modification();
            	break;
            case VueNavigation::AFF_PARTICIPATION :
                $content = $this->afficher_participation();
                break;
            case VueNavigation::AFF_SUPPRESSION:
            	$content = $this->suppression();
            	break;
        }
        return VuePageHTML::getHeaders().$content.VuePageHTML::getFooter();
    }

    /**
     * @return string
     * 
     * Change l'affichage en fonction de si une personne est connectée ou non
     */
    private function index(){
    	if(isset($_SESSION['email'])){
            $index = $this->indexCo();
        } else {
            $index = $this->indexPasCo();
        }
        return $index.$this->indexBase();
    }

    /**
     * @return string
     * 
     * Affichage de base du site (commun aux deux cas)
     */
    private function indexBase(){
        $app = \Slim\Slim::getInstance();
        $r_afficher_liste_m = $app->urlFor("afficherUneListeM");
        $r_listes_publique = $app->urlFor('listesPubliques');
        $r_liste_createur = $app->urlFor("listeCreateurs");
        return <<<end
        <br>
        <a href=$r_afficher_liste_m> Afficher une liste</a> <br>
        <a href="$r_listes_publique">Voir les listes publiques</a><br>
		<a href="$r_liste_createur">Voir les listes des créateurs</a>
end;


    }
    
    /**
     * @return string
     * 
     * Affichage cas où personne n'ets connecter sur ce navigateur
     */
    private function indexPasCo(){
        return <<<end
        <br>
        <h1>Bienvenue sur MyWishList</h1>
        <br>
        <p>Vous n'êtes pas connecté.</p>
end;
    }
    
    /**
     * @return string
     * 
     * Affichage cas où un utilisateur est connecté
     */
    private function indexCo(){
        $app = \Slim\Slim::getInstance();
        $mail = $_SESSION['email'];
        $r_partic = $app->urlFor('participation');
        $r_listes = $app->urlFor('mesListes');
        return <<<end
        <br>
        <h1>Bienvenue sur MyWishList</h1>
        <br>
        <p>Vous êtes connecté en tant que $mail.</p>
        <a href="$r_partic">Voir mes participations</a><br>
        <a href="$r_listes">Voir mes listes</a>
end;
    }


    /**
     * @return string
     * 
     * Fonction pour affichager la page de connexion
     */
    private function connexion(){
        $app = \Slim\Slim::getInstance();
        $r_connexion = $app->urlFor("connexion");
        $r_accueil = $app->urlFor("accueil");
        return  <<<end
        <h1>Page de connexion</h1>
        <br>
        <br>
        <form id="form_connexion" class="formulaire" method="POST" action="$r_connexion">
            <div class="row">
                <div class="input-field">
                    <label class="black-text">Adresse mail</label>
                    <input placeholder="exemple@mail.com" type="email" name="mailCo" id="connexion_mail" required>
                </div>
                <br/>
                 <div class="input-field">
                     <label class="black-text">Mot de passe</label>
                    <input placeholder="mot de passe"  type="password" name="mdpCo" id="connexion_mdp" required>
                 </div>
             </div>
            <br/><br/>
            <button type="submit" name="Se connecter" value="formCo">Se connecter</button>
        </form>
		<br>
		<a href='$r_accueil' id="accueil" >Accueil</a>
end;

    }

    /**
     * @return string
     * 
     * Affichage de la page d'inscription
     */
    private function inscription(){
        $app = \Slim\Slim::getInstance();
        $r_inscription = $app->urlFor("inscription");
        $r_accueil = $app->urlFor("accueil");
        $affichage =  <<<end
        <h1>Page d'inscription</h1>
        <br/>
        <br/>
		<form id="formulaire_inscription" class="for" method="POST" action="$r_inscription">
        <div class="row">
			<label class="black-text">Nom d'utilisateur</label>
            <div class="input-field">
                <input placeholder="Nom"  type="text" name="nomInscr" id="inscription_nom" required>
            </div>
            <br/>
			<label class="black-text">Prénom d'utilisateur</label>
			<div class="input-field">
                <input placeholder="Prénom"  type="text" name="prenomInscr" id="inscription_prenom" required>
            </div>
            <br/>
            <label class="black-text">Adresse mail</label>
            <div class="input-field">
                <input placeholder="JohnJohny@gmail.com" type="email" name="mailInscr" id="inscription_mail" required>
            </div>
			<br/>
            <label class="black-text">Date de naissance (JJ/MM/AAAA)</label>
			<div class="input-field">
                <select id="inscription_jour" name="jour">
end;
            for ($i=1; $i<=31; $i++) {
                $affichage = $affichage . "<option value=\"$i\">$i</option>\n";
            }
            $affichage = $affichage . "</select><select id='inscription_mois' name='mois'>";
            for ($i=1; $i<=12; $i++) {
                $affichage = $affichage . "<option value=\"$i\">$i</option>\n";
            }
            $affichage = $affichage . "</select><select id='inscription_annee' name='annee'>";
            for ($i=date('Y'); $i>=date('Y')-100; $i--) {
                $affichage = $affichage . "<option value=\"$i\">$i</option>\n";
            }
            $affichage = $affichage . <<<end
</select>
            </div>
            <br/>
			<label class="black-text">Mot de passe (taille de 6 à 20 charactères)</label>
            <div class="input-field">
                <input placeholder="**********"  type="password" name="mdp1Inscr" id="inscription_mdp1" pattern=".{6,20}" required>
            </div>
            <br/>
			<label class="black-text">Confirmation du mot de passe</label>
            <div class="input-field">
                <input placeholder="**********"  type="password" name="mdp2Inscr" id="inscription_mdp2" pattern=".{6,20}" required>
            </div>
			<br/>
			<input type="submit" value="S'inscrire">
        </div>
        <br/>
        <a href=" $r_accueil" id="accueil" >Accueil</a>
    </form>
end;
return $affichage;
    }
    
    /**
     * @return string
     * 
     * Affichage de la page de modification de compte
     */
    private function modification(){
    	$app = \Slim\Slim::getInstance();
    	$r_modification = $app->urlFor("modification");
    	$r_accueil = $app->urlFor("accueil");
    	$r_confirmationSuppression = $app->urlFor("suppressionCompte");
    	if(isset($_SESSION['email'])){
    		$mail = $_SESSION['email'];
    	}else{
    		$app->redirect('connexion');
    	}
    	$user = Utilisateur::getByEmail($mail);
    	list($annee,$mois,$jour) = explode('-',$user->date);
    	$affichage =  <<<end
        <h1>Profil de l'utilisateur</h1>
        <br/>
        <br/>
		<form id="formulaire_inscription" class="for" method="POST" action="$r_modification">
        <div class="row">
			<label class="black-text">Nom d'utilisateur</label>
            <div class="input-field">
                <input placeholder="Nom" value='$user->nom' type="text" name="nomInscr" id="inscription_nom" required>
            </div>
            <br/>
			<label class="black-text">Prénom d'utilisateur</label>
			<div class="input-field">
                <input placeholder="Prénom" value='$user->prenom' type="text" name="prenomInscr" id="inscription_prenom" required>
            </div>
            <br/>
            <label class="black-text">Adresse mail</label>
            <div class="input-field">
                <input placeholder="JohnJohny@gmail.com" value='$user->email' type="email" name="mailInscr" id="inscription_mail" required>
            </div>
			<br/>
            <label class="black-text">Date de naissance (JJ/MM/AAAA)</label>
			<div class="input-field">
                <select id="inscription_jour" name="jour">
end;
    	for ($i=1; $i<=31; $i++) {
    		if($i == $jour){
    			$affichage = $affichage . "<option value=\"$i\" selected>$i</option>\n";
    		} else {
    			$affichage = $affichage . "<option value=\"$i\">$i</option>\n";
    		}
    	}
    	$affichage = $affichage . "</select><select id='inscription_mois' name='mois'>";
    	for ($i=1; $i<=12; $i++) {
    		if($i == $mois){
    			$affichage = $affichage . "<option value=\"$i\" selected>$i</option>\n";
    		} else {
    			$affichage = $affichage . "<option value=\"$i\">$i</option>\n";
    		}
    	}
    	$affichage = $affichage . "</select><select id='inscription_annee' name='annee'>";
    	for ($i=date('Y'); $i>=date('Y')-100; $i--) {
    		if($i == $annee){
    			$affichage = $affichage . "<option value=\"$i\" selected>$i</option>\n";
    		} else {
    			$affichage = $affichage . "<option value=\"$i\">$i</option>\n";
    		}
    	}
    	$affichage = $affichage . <<<end
</select>
            </div>
            <br/><br/>
			<label class="black-text">Modification du mot de passe :</label><br/>
			<label class="black-text">Ancien mot de passe</label>
            <div class="input-field">
                <input placeholder="*********"  type="password" name="mdp0Inscr" id="inscription_mdp0" pattern=".{6,20}">
            </div>
            <br/>
			<label class="black-text">Nouveau mot de passe (taille de 6 à 20 charactères)</label>
            <div class="input-field">
                <input placeholder="*********"  type="password" name="mdp1Inscr" id="inscription_mdp1" pattern=".{6,20}">
            </div>
            <br/>
			<label class="black-text">Confirmation du mot de passe</label>
            <div class="input-field">
                <input placeholder="*********"  type="password" name="mdp2Inscr" id="inscription_mdp2" pattern=".{6,20}">
            </div>
        </div>
        <br/><br/>
        <button type="submit" name="inscription" value="formCo">Modifier</button>
    </form>
	<br>
	<a href=" $r_accueil" id="accueil" >Accueil</a><br>
	<label class="black-text">Si vous le souhaitez, vous pouvez supprimer votre compte en clickant sur le lien : </label>
	<a href=" $r_confirmationSuppression" id="confirmationSuppression" >Suppression de compte</a>
end;
		return $affichage;
    }

    /**
     * @return string
     * 
     * Affichage de la liste des participations
     */
    private function afficher_participation(){
        if(isset($_SESSION['email'])){
            $user = Utilisateur::getByEmail($_SESSION['email']);
            $participations = $user->participations;
            if(count($participations)>0){
                $content = "<h1>Voici la liste de vos participations : </h1>";
                foreach ($participations as $participation){
                    $i = Item::getByID($participation->id_item);
                    $l = Liste::getByID($i->liste_id);
                    if(isset($participation->id_message)){
                        $id_m = $participation->id_message;
                        $m = MessageListe::getByID($id_m);
                        $content .=  "L'item $i->nom, de la liste $l->titre avec comme message que vous avez laissé : $m->message<br>";
                    } else {
                        $content .= "L'item $i->nom, de la liste $l->titre<br>";
                    }
                }
            } else {
                $content = "<h1>Vous n'avez pas encore de participations</h1>";
            }
        }
        return $content;
    }
    	
    /**
     * @return string
     * 
     * Méthode pour supprimer
     */
    private function suppression(){
    	$app = \Slim\Slim::getInstance();
    	$r_suppression = $app->urlFor("suppressionComptePost");
    	$retour = $_SERVER["HTTP_REFERER"];
        return <<<end
		<label class="black-text">Êtes-vous sûr de vouloir supprimer votre compte? Cette action est irréversible.</label><br>
		<form id="formulaire_suppression" class="for" method="POST" action="$r_suppression">
			<button type="submit" name="suppression" value="formCo">Oui, je veux supprimer</button>
		</form>
		<a href="$retour">Non, je souhaite revenir à la page d'avant</a>		
end;
    }
}