<?php
/**
 * Created by PhpStorm.
 * User: Clément Teyssandier
 * Date: 05/01/2018
 * Time: 23:44
 */

namespace mywishlist\vues;

use mywishlist\models\Item;
use mywishlist\models\Liste;
use mywishlist\models\MessageListe;
use mywishlist\models\Utilisateur;

class VueItem
{

    const AFF_UN_ITEM = 1;
    const AFF_AJOUT_ITEM = 2;
    const AFF_MODIF_ITEM = 3;
    const AFF_MODIF_IMAGE_M = 4;
    const AFF_UP_IMAGE_M = 5;
    const AFF_CAGNOTTE_EDITION = 6;
    const AFF_CAGNOTTE_PARTICIPATION = 7;

    /**
     * @param unknown $selecteur
     * @param unknown $num
     * @return string
     * 
     * Méthode pour afficher en fonciton du cas
     */
    public function render($selecteur,$num = null)
    {
        $content=null;
        switch ($selecteur) {
            case VueItem::AFF_UN_ITEM :
                $content = $this->un_Item($num);
                break;
            case VueItem::AFF_AJOUT_ITEM :
            	$content = $this->ajoutItem($num);
            	break;
            case VueItem::AFF_MODIF_ITEM :
            	$content = $this->modifItem($num);
            	break;
            case VueItem::AFF_MODIF_IMAGE_M :
                $content = $this->modifierImageM($num);
                break;
            case VueItem::AFF_UP_IMAGE_M :
                $content = $this->uploadImage_M($num);
                break;
            case VueItem::AFF_CAGNOTTE_EDITION :
            	$content = $this->editionCagnotte($num);
            	break;
            case VueItem::AFF_CAGNOTTE_PARTICIPATION :
            	$content = $this->participationCagnotte($num);
            	break;
        }
        return VuePageHTML::getHeaders().$content.VuePageHTML::getFooter();
    }
    
    /**
     * Méthode d'affichage de la participation à une cagnotte
     * @param unknown $num
     * @return string
     */
    private function participationCagnotte($num){
    	$app = \Slim\Slim::getInstance();
    	$retour = $app->urlFor('listenum',['no'=>$num[3]->token]);
    	$cagn = $app->urlFor("ajoutCagnotte");
    	$item = $num[1];
    	$id = $item->id;
    	$tarif = $item->tarif;
    	$val = $num[2]->valeur;
    	$restant = $tarif-$val;
		$affichage = "<h1>Menu de la cagnotte sur l'objet $item->id</h1><br>";
		if($restant<=0){
			$affichage .= <<<end
			<label class="black-text">Le montant total de la cagnotte a été atteinte.</label><br><br>
end;
		} else {
			$token = $num[3]->token;
			$cagnotte = $num[2]->id_cagn;
			$affichage .= <<<end
			<form id="form_une_liste_m" class="formulaire" method="POST" action="$cagn">
			<br>
			<h2>Cagnotte actuelle : $val<h2>
			<label class="black-text">Insérez la valeur que vous voulez ajouter à la cagnotte (maximum $restant €)</label><br>
			<input placeholder="Valeur" type="number" step="0.01" min="0" max="$tarif-$val" name="valeurCagnote" id="valeurCagnote" required><br>
			<button type="submit" name="valeurC" >Ajouter de la valeur à la cagnotte</button>
			<input id="token" name="token" type="hidden" value=$token>
			<input id="idItem" name="idItem" type="hidden" value=$id>
			<input id="cagnotte" name="cagnotte" type="hidden" value=$cagnotte>
        	</form><br><br>
end;
		}
		$affichage.='<a href='.$retour.'>Retour</a>';
		return $affichage;
    }
    
    /**
     * @param unknown $num
     * @return string
     * 
     * Méthode pour éditer la Cagnotte
     */
    private function editionCagnotte($num){
    	$app = \Slim\Slim::getInstance();
    	$liste = $num[0];
    	$idL = $liste->no;
    	$item = $num[1];
    	$idI = $item->id;
    	$dateItem = $liste->expiration;
    	$dateAct = date("Y-m-d");
    	$redirection = $app->urlFor("creationCagnotte");
    	$cagnotte = $item->id_cagn;
    	$content = "<h1>Menu création de cagnotte</h1><br>";
    	if(sizeof($cagnotte) == 0 || $cagnotte == 0){
    		$content .= <<<end
			<label class="black-text">Il est possible de lancer une cagnotte si vous estimez que l'item paraît être trop cher.</label><br>
			<form id="form_une_liste_m" class="formulaire" method="POST" action="$redirection">
            	<br>
				<label class="black-text">Attention : dès lors que la cagnotte est lancée, il n'est plus possible de l'annuler.</label><br>
            	<button type="submit" name="Voir_Liste" value="formUneListeM">Créer une cagniotte sur l'item $item->id</button>
				<input id="idListe" name="idListe" type="hidden" value=$idL>
				<input id="idItem" name="idItem" type="hidden" value=$idI>
        	</form>
end;
    	} else {
    		if($dateItem>$dateAct){
    			$retour = $app->urlFor('edition',['no'=>$idL]);
    			$content .= <<<end
				<label class="black-text">Une cagnotte est lancée. La date n'est pas encore expiré.</label><br>
            	<br>
            	<a href='$retour'>Retour</a>
end;
    		} else {
    			$retour = $app->urlFor('edition',['no'=>$idL]);
    			$content .= <<<end
				<label class="black-text">Une cagnotte est lancée. La date est expirée</label><br>
            	<br>
            	<a href='$retour'>Retour</a>
end;
    		}
    	}
    	return $content;
    }

    /**
     * @param unknown $num
     * @return string
     * 
     * Méthode pour afficher un item et ses attributs
     */
    private function un_Item($num){
        $item = Item::getByID($num);

        $content = "<h1>Item n $num</h1>";
        if($item->img != ""){
            $content .= "<br><img src=\"../doc/Images/$item->img\" alt=\"Image de l'item\" id=\"idImageItem\"/> <br>";
        }
        $content .= "<br><p> Nom  : $item->nom </p>
                    <p> Description  : $item->descr </p>
                    <p> Appartient à la liste : $item->liste_id </p>";
        if($item->url != ""){
            $content .= "<br><a href=\"$item->url\">l'url</a>";
        }

        if(isset($_SESSION['email'])){
            $content .= $this->itemCo($num);
        } else {
            $content .= $this->itemPasCo($num);
        }
        return $content;
    }

    /**
     * @param unknown $item_id
     * @return unknown
     * 
     * Affichage des items quand vous etes connecté
     */
    private function itemCo($item_id){
        $item = Item::getByID($item_id);
        $liste = Liste::where('no', '=', $item->liste_id)->first();
        $createur = Utilisateur::getByUserID($liste->user_id);
        $mail_createur = $createur->email;
        if($mail_createur == $_SESSION['email']){
            $content = $this->itemCoCreateur($item_id);
        } else {
            $content = $this->itemCoPasCreateur($item_id);
        }
        return $content;
    }

    /**
     * @param unknown $item_id
     * @return string
     * 
     * Affichage des items quand vous etes connecté et que vous etes le créateur
     */
    private function itemCoCreateur($item_id){
        $item = Item::getByID($item_id);
        $liste = Liste::where('no', '=', $item->liste_id)->first();
        $createur = Utilisateur::getByUserID($liste->user_id);
        $mail_createur = $createur->email;
        $u = Utilisateur::getByEmail($mail_createur);
        $dateCurrent = date("Y-m-d");
        $dateExpiration = $liste->expiration;
        if(isset($item->participant)){
            if($u->pour_proche == 0){
                $dateCurrent = date("Y-m-d");
                $dateExpiration = $liste->expiration;
                $dateExpiration = strtotime($dateExpiration);
                $dateCurrent = strtotime($dateCurrent);
                if($dateExpiration - $dateCurrent < 0){
                    $content = "<b>Avec comme participant : </b> $item->participant";
                    if(isset($item->id_message)){
                        $m = MessageListe::getByID($item->id_message);
                        $content .= "<b>Avec comme message </b>".$m->message;
                    }
                } else {
                    $content = "<b>Il y a un participant mais c'est une surprise</b>";
                }
            } else {
                $content = "<b>Avec comme participant : </b> $item->participant";
                if(isset($item->id_message)){
                    $m = MessageListe::getByID($item->id_message);
                    $content .= "<p><b>Avec comme message </b>".$m->message;
                }
            }
        } else {
            $content = "<p>Il n'y a pas de participant</p>";
            if($u->pour_proche != 0){
                $content .= $this->reserverItemCo($item_id);
            }
        }
        if($item->img == ""){
            $content .= $this->ajoutImageInput($item_id);
        } else {
            $content .=  $this->supprimerImage($item_id).$this->modifierImage($item_id);
        }

        $content .= $this->uploadImage($item_id).$this->form_supprimer($item_id);
        return $content;
    }

    /**
     * @param unknown $item_id
     * @return string
     * 
     * Affichage des items quand vous etes connecté mais que vous n'etes pas le créateur
     */
    private function itemCoPasCreateur($item_id){
        $item = Item::getByID($item_id);
        if(isset($item->participant)){
            $content = "<p><b>Avec comme participant :</b> $item->participant </p>";
            if(isset($item->id_message)){
                $m = MessageListe::getByID($item->id_message);
                $content .= $m->message;
            }
        } else {
            $content = $this->reserverItemCo($item_id);
        }
        return $content;
    }

    /**
     * @param unknown $item_id
     * @return string
     *
     * Affichage des items quand vous n'etes pas connecté
     */
    private function itemPasCo($item_id){
        $item = Item::getByID($item_id);
        if(isset($item->participant)){
            $content = "<p><b>avec comme participant :</b> '.$item->participant </p>";
            if(isset($item->id_message)){
                $m = MessageListe::getByID($item->id_message);
                $content .= $m->message;
            }
        } else {
            $content = $this->reserverItemPasCo($item_id);
        }
        return $content;
    }

    /**
     * @param unknown $item_id
     * @return string
     * 
     * Méthdoe pour réserver un item une fois connecter 
     */
    private function reserverItemCo($item_id){
        $app = \Slim\Slim::getInstance();
        $r_reserver = $app->urlFor("reserverItem");
        return <<<end
        <h3>Vous pouvez réserver cet objet et ajouter un message</h3>
        <form id="form_reservation" class="formulaire" method="POST" action="$r_reserver">
            <div class="row">
                <div class="input-field">
                    <label class="black-text">Message</label>
                    <input placeholder="Votre message (optionnel)" type="text" name="messageRes" id="message_reservation">
                    <input id="itemId" name="itemId" type="hidden" value=$item_id>
                </div>
             </div>
            <br/><br/>
            <button type="submit" name="reserver" value="formRes">Réserver</button>
        </form>
end;
    }

    /**
     * @param unknown $item_id
     * @return string
     * 
     * Méthode pour réserver une item quand non connecté
     */
    private function reserverItemPasCo($item_id){
        $app = \Slim\Slim::getInstance();
        $r_reserver = $app->urlFor("reserverItem");
        return <<<end
        <h3>Vous pouvez réserver cet objet et ajouter un message</h3>
        <form id="form_reservation" class="formulaire" method="POST" action="$r_reserver">
            <div class="row">
                <div class="input-field">
                    <label class="black-text">Nom</label>
                    <input placeholder="Votre nom" type="text" name="nomRes" id="nom_reservation" required>
                    <br>
                    <br>
                    <label class="black-text">Message</label>
                    <input placeholder="Votre message (optionnel)" type="text" name="messageRes" id="message_reservation">
                    <input id="itemId" name="itemId" type="hidden" value=$item_id>
                </div>
             </div>
            <br/><br/>
            <button type="submit" name="reserver" value="formRes">Réserver</button>
        </form>
end;
    }

    /**
     * @param unknown $item_id
     * @return string
     * 
     * Méthode pour afficher un formulaire pour supprimer un item
     */
    private function form_supprimer($item_id){
        $app = \Slim\Slim::getInstance();
        $r_supprimer = $app->urlFor("supprimerItem");
        return <<<end
        <br><br>
        <form id="form_suppression" class="formulaire" method="POST" action="$r_supprimer">
            <input id="itemId" name="itemIdSupp" type="hidden" value=$item_id>
            <button type="submit" name="form_supprimer" value="formSupItem">Supprimer l'item</button>
        </form>
end;

    }

    /**
     * @param unknown $item_id
     * @return string
     * 
     * Méthode pour afficher le formulaire de suppression d'une image
     */
    private function supprimerImage($item_id){
        $app = \Slim\Slim::getInstance();
        $r_supprimerImage = $app->urlFor("supprimerImage");
        return <<<end
        <form id="form_suppressionImage" class="formulaire" method="POST" action="$r_supprimerImage">
            <input id="itemIdSupp" name="itemIdSupp" type="hidden" value=$item_id>
            <button type="submit" name="supprimerImage" value="formSupItem">Supprimer l'image</button>
        </form>
end;

    }

    /**
     * @param unknown $num
     * @return string
     * 
     * Affichage d'un formulaire pour l'ajout d'un item
     */
    private function ajoutItem($num){
    	$app = \Slim\Slim::getInstance();
    	$enrengistrer = $app->urlFor("ajouterItem");
    	return <<<end
        <form id="form_reservation" class="formulaire" method="POST" action="$enrengistrer">
			<div class="input-field">
                <label class="black-text">Entrez le nom de l'item : </label>
                <input placeholder="" type="text" name="nomItem" id="nom_Item" required>
            </div>
			<br>
			<div class="input-field">
                <label class="black-text">Entrez un descriptif à l'item : </label>
                <input placeholder="" type="text" name="descrItem" id="descr_Item" required>
            </div>
			<br>
			<div class="input-field">
                <label class="black-text">Entrez un prix à l'item : </label>
                <input placeholder="" type="number" step="0.01" min="0" name="prixItem" id="prix_Item" required>
            </div>
			<br>
			<input id="idListe" name="idListe" type="hidden" value=$num>
            <button type="submit" name="ajouterItem" value="ajouterItem">Ajouter l'item</button>
        </form>
end;
    }

    /**
     * @param unknown $param
     * @return string
     * 
     * Affichage d'un formulaire pour la modification d'un item
     */
    public function modifItem($param)
    {
        $app = \Slim\Slim::getInstance();
        $redirection = $app->urlFor("editionItemPost");
        $retour = $app->urlFor('edition', ['no' => $param[0]]);
        $idListe = $param[0];
        $item = $param[1];
        return <<<end
		<form id="form_reservation" class="formulaire" method="POST" action="$redirection">
			<div class="input-field">
                <label class="black-text">Entrez le nom de l'item : </label>
                <input placeholder="" type="text" name="nomItem" id="nom_Item" value="$item->nom" pattern=".{3,50}" required>
            </div>
			<br>
			<div class="input-field">
                <label class="black-text">Entrez un descriptif à l'item : </label>
                <input placeholder="" type="text" name="descrItem" id="descr_Item" value="$item->descr" pattern=".{0,100}">
            </div>
			<br>
			<div class="input-field">
                <label class="black-text">Entrez un prix à l'item : </label>
                <input placeholder="" type="number" step="0.01" min="0" name="prixItem" id="prix_Item" value="$item->tarif"required>
            </div>
			<br>
			<input id="idListe" name="idListe" type="hidden" value=$idListe>
			<input id="idItem" name="idItem" type="hidden" value=$item->id>
            <button type="submit" name="ajouterItem" value="ajouterItem">Modifier l'item</button>
        </form>
		<a href=$retour>Retour</a>
end;
    }

    /**
     * @param unknown $num
     * @return string
     * 
     * Affichage du formulaire pour ajouter le nom d'une image dans la base
     */
    private function ajoutImageInput($num){
        $app = \Slim\Slim::getInstance();
        $r_ajout_image = $app->urlFor("ajoutImageItem");
        return <<<end
        <h1>Ajout de l'image d'un item</h1>

        <form id="form_ajout_image" class="formulaire" method="POST" action="$r_ajout_image">
            <div class="row">
                <div class="input-field">
                    <label class="black-text">Entrez le nom de l'image </label>
                    <input placeholder="image.png" type="text" name="imageAj" id="imageAj" required>
                    <input id="itemIdAj" name="itemIdAj" type="hidden" value=$num>
                </div>
             </div>
            <button type="submit" name="formAjImageM" value="formAjImageM">Ajouter l'image</button>
        </form>

end;
    }

    /**
     * @return string
     * 
     * Affichage du menu pour modifier une image
     */
    private function modifierImageM(){
        $app = \Slim\Slim::getInstance();
        $requete = $app->request();
        $id_item = $requete->post("itemIdMod");
        $r_modifier_image = $app->urlFor("modifierImage");
        return <<<end
        <h1>Modifier l'image d'un item</h1>

        <form id="form_mod_image" class="formulaire" method="POST" action="$r_modifier_image">
            <div class="row">
                <div class="input-field">
                    <label class="black-text">Entrez le nom de l'image </label>
                    <input placeholder="image.png" type="text" name="imageMod" id="imageMod" required>
                    <input id="itemIdMod" name="itemIdMod" type="hidden" value=$id_item>
                </div>
             </div>
            <button type="submit" name="formModImage" value="formModImage">Modifier l'image</button>
        </form>

end;
    }

    /**
     * @param unknown $num
     * @return string
     * 
     * Formulaire pour modifier une image
     */
    private function modifierImage($num){
        $app = \Slim\Slim::getInstance();
        $r_modifier_image = $app->urlFor("modifierImageM");
        return <<<end
        <form id="form_modif_image" class="formulaire" method="POST" action="$r_modifier_image">
            <div class="row">
                <div class="input-field">
                    <input id="itemIdMod" name="itemIdMod" type="hidden" value=$num>
                </div>
             </div>
            <button type="submit" name="formModImageM" value="formModImageM">Modifier l'image</button>
        </form>
end;
    }

    /**
     * @param unknown $num
     * @return string
     * 
     * Formulaire pour uploader une image
     */
    private function uploadImage($num){
        $app = \Slim\Slim::getInstance();
        $r_upload = $app->urlFor("uploadImageM");
        return <<<end
        <form id="form_upload_image" class="formulaire" method="POST" action="$r_upload">
            <div class="row">
                <div class="input-field">
                    <input id="itemIdUp" name="itemIdUp" type="hidden" value=$num>
                </div>
             </div>
            <button type="submit" name="formUpImageM" value="formUpImageM">Uploader l'image</button>
        </form>
end;
    }

    /**
     * @return string
     * 
     * Menu d'upload d'image
     */
    private function uploadImage_M(){
        $app = \Slim\Slim::getInstance();
        $requete = $app->request();
        $id_item = $requete->post('itemIdUp');
        $r_upload = $app->urlFor("uploadImage");
        return <<<end
        <form method="post" enctype="multipart/form-data" action="$r_upload">
            <label>Ajoutez une image :  </label><br/>
            <input type="file" name="uploadIm"/>
            <input id="itemIdUpM" name="itemIdUpM" type="hidden" value=$id_item>
            <button type="submit" name="formUpImage" value="formUpImage">Uploader l'image</button>
</form>
end;
    }
}
