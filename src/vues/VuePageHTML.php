<?php

namespace mywishlist\vues;

class VuePageHTML{
	
	public static function getHeaders(){
		print self::headersDeb();
		if(isset($_SESSION['email'])){
			print self::headersCo();
		}else{
			print self::headersPasCo();
		}
		print self::finHeader();
	}
	
	/**
	 * @return string
	 * 
	 * HTML du Début de chaque page (header)
	 */
	public static function headersDeb(){
		$app =  \Slim\Slim::getInstance();
		$index = $app->urlFor('accueil');
		$requete = $app->request();
		$path = $requete->getRootUri();
		$img =$path.'/doc/Images/Logo.png';
		$favi=$path.'/doc/Images/favicon.png';
		$css = $path.'/web/css/style.css';
		return <<<end
<!DOCTYPE html>
<html>
    <head>
         <meta charset="utf-8" />
        <title>MyWishList</title>
        <link rel="stylesheet" type="text/css" href="$css" />
        <link rel="icon" type="image/png" href="$favi" />
    </head>
    <body>
        <header>
            <a href="$index"> <img src="$img" alt="Logo My WishList" id="imglogo"/> </a>
end;

}

/**
 * @return string
 * 
 * Appel HMTL d'affichage Header si un utilisateur est co
 */
public static function headersCo(){
        $app =  \Slim\Slim::getInstance();
	    $adr = $app->urlFor('deconnexion');
	    $r_profil = $app->urlFor('modification');
		return <<<end
			<a href="$r_profil">Profil</a>
			<a href="$adr">Se déconnecter</a>
end;
	}
	
	/**
	 * @return string
	 * 
	 * Appel HMTL d'affichage Header si un utilisateur n'est pas co
	 */
	public static function headersPasCo(){
		$app =  \Slim\Slim::getInstance();
		$co = $app->urlFor('connexion');
		$inscr = $app->urlFor("inscription");
		return <<<end
	<a href="$co">Se connecter</a>
	<a href="$inscr">S'inscrire</a>
end;
	}
	
	/**
	 * @return string
	 * 
	 * HTML pour finir le header
	 */
	public static function finHeader(){
		return <<<end
	</header>
	<main>
end;
	}
	
	/**
	 * @return string
	 * 
	 * Affichage du footer sur chaque page
	 */
	public static function getFooter(){
		return <<<end
    </main>
    <footer>
        <ul>
            <li>Clément Teyssandier</li>
            <li>Ian Bialo</li>
            <li>David Holzhammer</li>
            <li>Théo Fraschini</li>
            <li>IUT Charlemagne</li>
        </ul>
    </footer>
</body>
</html>
end;
	}
}